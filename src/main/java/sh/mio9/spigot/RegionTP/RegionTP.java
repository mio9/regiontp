package sh.mio9.spigot.RegionTP;

import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.annotation.dependency.Dependency;
import org.bukkit.plugin.java.annotation.plugin.Description;
import org.bukkit.plugin.java.annotation.plugin.Plugin;
import sh.mio9.spigot.RegionTP.command.Commands;
import sh.mio9.spigot.RegionTP.command.ReloadCommand;


@Plugin(name = "RegionTP",version = "1.0-SNAPSHOT")
@Description("Teleport player in a region")
@Dependency("WorldGuard")

public class RegionTP extends JavaPlugin {

    @Override
    public void onEnable() {
        this.getLogger().info("Loading config....");
        this.saveDefaultConfig();
        this.getLogger().info("Config OK :)");
        this.getLogger().info("Registering commands...");
        this.getCommand("regiontp").setExecutor(new Commands(this));
        this.getCommand("rtpreload").setExecutor(new ReloadCommand(this));
        this.getLogger().info("Commands OK :)");
        // assume no other stuff
        this.getLogger().info("RegionTP had been enabled");
    }

    @Override
    public void onDisable() {

        this.getLogger().info("RegionTP had been disabled");
    }
}

package sh.mio9.spigot.RegionTP.command;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import sh.mio9.spigot.RegionTP.RegionTP;

@org.bukkit.plugin.java.annotation.command.Command(name = "rtpreload", desc = "Reload RegionTP")
public class ReloadCommand implements CommandExecutor {
    private RegionTP plugin;


    public ReloadCommand(RegionTP plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        plugin.reloadConfig();
        commandSender.sendMessage(ChatColor.translateAlternateColorCodes('&',"&aReloaded RegionTP config"));
        return true;
    }
}

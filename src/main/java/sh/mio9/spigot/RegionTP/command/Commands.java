package sh.mio9.spigot.RegionTP.command;

import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import sh.mio9.spigot.RegionTP.RegionTP;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@org.bukkit.plugin.java.annotation.command.Command(name = "regiontp", desc = "Teleport players in a region to coordinate")
public class Commands implements CommandExecutor {

    private RegionTP plugin;

    public Commands(RegionTP plugin) {
        this.plugin = plugin;
    }

    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        // check if region exists
        if (args.length == 0) {
            sender.sendMessage(ChatColor.YELLOW + "/regiontp <region-id> <x> <y> <z> [pitch] [yaw]");
            return true;
        }
        if (args[0] == null) {
            sender.sendMessage(ChatColor.RED + "Please enter a region");
            return true;
        }
        //check if coord is fine
        if (args.length < 4) {
            sender.sendMessage(ChatColor.RED + "Please enter a valid/complete coordinate");
            return true;
        }

        //RegionContainer container = WGBukkit.getInstance().getPlatform().getRegionContainer();
        World world;
        if (sender instanceof BlockCommandSender) {
            world = ((BlockCommandSender) sender).getBlock().getWorld();
        } else {
            world = ((Player) sender).getWorld();
        }

        RegionManager regions = WGBukkit.getRegionManager(world);
        if (regions == null || !regions.hasRegion(args[0])) {
            sender.sendMessage(ChatColor.RED + "No region found with name " + ChatColor.GOLD + args[0]);
            return true;
        }

        tpPlayersInRegion(sender, args[0], regions, Double.parseDouble(args[1]), Double.parseDouble(args[2]), Double.parseDouble(args[3]), args.length < 5 ? null : Float.parseFloat(args[4]), args.length < 6 ? null : Float.parseFloat(args[5]));


        return true;

    }

    private void tpPlayersInRegion(CommandSender sender, final String regionID, RegionManager regions, double destX, double destY, double destZ, Float destPitch, Float destYaw) {

        Collection<? extends Player> onlinePlayers = Bukkit.getOnlinePlayers();
        ArrayList<Player> playersInRegion = new ArrayList<>();

        onlinePlayers.forEach(player -> {
            Set<ProtectedRegion> regionsOfPlayer = regions.getApplicableRegions(player.getLocation()).getRegions();
            Set<ProtectedRegion> matchedRegions = regionsOfPlayer.stream().filter(region -> region.getId().equals(regionID)).collect(Collectors.toSet());
            if (!matchedRegions.isEmpty()) {
                if (destPitch == null || destYaw == null) {
                    player.teleport(new Location(player.getWorld(), destX, destY, destZ, player.getLocation().getYaw(), player.getLocation().getPitch()));
                } else {
                    player.teleport(new Location(player.getWorld(), destX, destY, destZ, destYaw, destPitch));
                }

                playersInRegion.add(player);
            }

        });
        ArrayList<String> playernamesInRegion = new ArrayList<>();
        playersInRegion.forEach(player -> playernamesInRegion.add(player.getName()));
        String destCoord = "(" + destX + ", " + destY + ", " + destZ + ", " + destPitch + ":" + destYaw + ")";
        String tpedPlayerCount = String.valueOf(playersInRegion.size());


        //sender.sendMessage(ChatColor.GREEN + "Teleported " + ChatColor.YELLOW +  + ChatColor.GREEN + " players in " + ChatColor.YELLOW + regionID + ChatColor.GREEN + " to " + ChatColor.YELLOW + "(" + destX + ", " + destY + ", " + destZ + ", " + destPitch + ":" + destYaw + ")");
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&',plugin.getConfig().getString("teleport-message").replace("{{count}}",tpedPlayerCount).replace("{{dest}}",destCoord).replace("{{list}}", Arrays.toString(playernamesInRegion.toArray()))));
    }
}

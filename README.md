# Region TP

tweaker plugin for teleporting players in worldguard region to a location

## Command Usage:
The one and only one command:
 - /regiontp \<region-id\> \<x\> \<y\> \<z\> \[pitch\] \[yaw\]

Reload Config:
- /rtpreload ([Config usage](src/main/resources/config.yml))



 
 > ***Notice:*** This plugin supports ONLY worldguard and worldedit for minecraft **1.12.X** versions
 > , 1.13 up is NOT (yet) supported. ClassNotFound exceptions will be thrown if used against 1.13 or above